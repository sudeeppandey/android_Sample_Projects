# List of sample projects:

:point_right: <a class="github-button" href="https://gitlab.com/sudeeppandey/Fire" aria-label="Follow @ntkme on GitHub">Firebase Job Scheduler</a> to schedule background long running tasks.

:point_right: <a class="github-button" href="https://gitlab.com/sudeeppandey/IntentService" aria-label="Follow @ntkme on GitHub">Started Service on Foreground</a> starts Service component on foreground.
using Android Notification.

:point_right: <a class="github-button" href="https://gitlab.com/sudeeppandey/AppExecutor" aria-label="Follow @ntkme on GitHub">AppExecutor</a> provides utility like class to initiate multi-threading.
tasks- UI thread, HandlerThread and ThreadPool.

:point_right: <a class="github-button" href="https://gitlab.com/sudeeppandey/Loader" aria-label="Follow @ntkme on GitHub">Loader</a> to populate UI data from Room Database.

:point_right: <a class="github-button" href="https://gitlab.com/sudeeppandey/PersistentBottomSheet" aria-label="Follow @ntkme on GitHub">Persistent Bottom Sheet</a> and <a class="github-button" href="https://gitlab.com/sudeeppandey/Dialog" aria-label="Follow @ntkme on GitHub">Dialog</a> from Material Design collection.

:point_right: <a class="github-button" href="https://gitlab.com/sudeeppandey/GoogleSignIn" aria-label="Follow @ntkme on GitHub">Google Sign In</a> for authentication.

:point_right: <a class="github-button" href="https://gitlab.com/sudeeppandey/ContentProvider" aria-label="Follow @ntkme on GitHub">Content Provider</a> for allowing data access to other applications.

:point_right: <a class="github-button" href="https://gitlab.com/sudeeppandey/Coordinator" aria-label="Follow @ntkme on GitHub">Coordinator Layout</a> - a UI sample.

:point_right: <a class="github-button" href="https://gitlab.com/sudeeppandey/SlidingPagesWithAnimation" aria-label="Follow @ntkme on GitHub">Slide Pages with Animation</a> using fragments.

:point_right: <a class="github-button" href="https://gitlab.com/sudeeppandey/Navigation" aria-label="Follow @ntkme on GitHub">Navigation</a> - with Navigation Drawer and ViewPager.

:point_right: <a class="github-button" href="https://gitlab.com/sudeeppandey/Notifications_Android" aria-label="Follow @ntkme on GitHub">Notifications</a> in android.

:point_right: <a class="github-button" href="https://gitlab.com/sudeeppandey/RecyclerView_CardView" aria-label="Follow @ntkme on GitHub">RecyclerView</a> with finite and infinite scrolling.

:point_right: <a class="github-button" href="https://gitlab.com/sudeeppandey/RoomDatabase" aria-label="Follow @ntkme on GitHub">Room</a> for application database persistence.

:point_right: <a class="github-button" href="https://gitlab.com/sudeeppandey/IntentImplicit" aria-label="Follow @ntkme on GitHub">Implicit Intent</a> to start camera application.


